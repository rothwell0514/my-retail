### Build Jar
use Maven wrapper to build jar: `./mvnw package`

### Start App
`java -jar target/product-api-0.0.1-SNAPSHOT.jar`

### Mongo
App assumes Mongo is running at localhost:27017.
Start Mongo using Docker:
`docker run --name mongo-myretail -d -p 27017:27017 mongo`

host/port can be changed in application.yml

### Swagger
You can access the Swagger page at localhost:8080/swagger-ui.html. From there, you can use the /price endpoint
to add pricing for a product. Then, use the /products endpoint get combined product info.

### curl
add price to an item:
`curl -X PUT "http://localhost:8080/price" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"currencyCode\": \"USD\", \"currentPrice\": 99.99, \"id\": \"13860428\"}"`

get product info:
`curl -X GET "http://localhost:8080/products/13860428" -H "accept: */*"`
