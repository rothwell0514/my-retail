package com.myretail.price;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/price")
public class PriceController {

    @Autowired
    ProductPriceRepository productPriceRepository;

    @GetMapping("/{productId}")
    public List<ProductPrice> getAll() {
        return productPriceRepository.findAll();
    }

    @PutMapping
    public ProductPrice put(@RequestBody ProductPrice productPrice) {
        return productPriceRepository.save(productPrice);
    }

    @DeleteMapping("/{productId}")
    public void delete(@PathVariable String productId) {
        productPriceRepository.deleteById(productId);
    }
}
