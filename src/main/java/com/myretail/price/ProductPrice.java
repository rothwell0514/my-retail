package com.myretail.price;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
public class ProductPrice {

    @Id
    private String id;
    private Double currentPrice;
    private String currencyCode;
}
