package com.myretail.product;

import com.myretail.exception.MissingDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/{productId}")
    public Product get(@PathVariable String productId) {
        return productService.getProduct(productId);
    }

    @ExceptionHandler(MissingDataException.class)
    public ResponseEntity handleMissingDataException() {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}
