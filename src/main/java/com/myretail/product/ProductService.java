package com.myretail.product;

import com.myretail.client.CatalogEntry;
import com.myretail.client.ProductCatalogClient;
import com.myretail.exception.MissingDataException;
import com.myretail.price.ProductPrice;
import com.myretail.price.ProductPriceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

@Service
@Slf4j
public class ProductService {

    @Autowired
    ProductCatalogClient productCatalogClient;

    @Autowired
    ProductPriceRepository productPriceRepository;

    @Autowired
    ExecutorService executor;

    public Product getProduct(String id) {
        Future<CatalogEntry> catalogFuture = executor.submit(() -> productCatalogClient.getCatalogEntry(id));
        Future<ProductPrice> priceFuture = executor.submit(() -> productPriceRepository.findById(id).get());

        CatalogEntry catalogEntry;
        ProductPrice productPrice;

        try {
            catalogEntry = catalogFuture.get();
        } catch (Exception e) {
            String msg = "Error getting catalog data for product ID " + id;
            log.warn(msg, e);
            throw new MissingDataException(msg);
        }

        try {
            productPrice = priceFuture.get();
        } catch (Exception e) {
            String msg = "Error getting price for product ID " + id;
            log.warn(msg);
            throw new MissingDataException(msg);
        }

        return new Product(
                id,
                catalogEntry.getProduct().getItem().getDescription().getTitle(),
                productPrice.getCurrentPrice(),
                productPrice.getCurrencyCode()
        );
    }
}
