package com.myretail.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
public class Product {
    private String id;
    private String name;
    private Double currentPrice;
    private String currencyCode;
}
