package com.myretail.client;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@AllArgsConstructor
public class CatalogEntry {

    private CatalogProduct product;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    @AllArgsConstructor
    public static class CatalogProduct {
        private CatalogItem item;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    @AllArgsConstructor
    public static class CatalogItem {
        @JsonProperty("tcin")
        private String id;

        @JsonProperty("product_description")
        private CatalogProductDescription description;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    @AllArgsConstructor
    public static class CatalogProductDescription {
        private String title;
    }
}
