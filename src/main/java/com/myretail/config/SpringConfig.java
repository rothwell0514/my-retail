package com.myretail.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class SpringConfig {

    @Bean("productCatalogTemplate")
    public RestTemplate productCatalogTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    ExecutorService executorService(@Value("${parallelRequest}") int parallelRequest) {
        return Executors.newFixedThreadPool(parallelRequest);
    }

}
