package com.myretail.price;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductPriceRepositoryTest {

    @Autowired
    private ProductPriceRepository productPriceRepository;

    @Test
    public void canInsertAndRetrieve() {
        ProductPrice pp = new ProductPrice("1", 1.11, "CAN");
        productPriceRepository.save(pp);

        Optional<ProductPrice> retrieved = productPriceRepository.findById("1");
        assertTrue(retrieved.isPresent());
        assertEquals(Double.valueOf(1.11), retrieved.get().getCurrentPrice());
        assertEquals("CAN", retrieved.get().getCurrencyCode());

    }
}