package com.myretail.product;

import com.myretail.client.CatalogEntry;
import com.myretail.client.CatalogEntry.CatalogItem;
import com.myretail.client.CatalogEntry.CatalogProductDescription;
import com.myretail.client.ProductCatalogClient;
import com.myretail.exception.MissingDataException;
import com.myretail.price.ProductPrice;
import com.myretail.price.ProductPriceRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

    @Mock
    ProductCatalogClient productCatalogClient;

    @Mock
    ProductPriceRepository productPriceRepository;

    @InjectMocks
    ProductService productService;

    @Before
    public void doBefore() {
        productService.executor = Executors.newSingleThreadExecutor();
    }

    @Test
    public void testProductFetching() {
        CatalogProductDescription cpd = new CatalogProductDescription("Test Project");
        CatalogItem ci = new CatalogItem("1234", cpd);
        CatalogEntry.CatalogProduct cp = new CatalogEntry.CatalogProduct(ci);
        CatalogEntry catalogEntry = new CatalogEntry(cp);
        when(productCatalogClient.getCatalogEntry("1234")).thenReturn(catalogEntry);

        ProductPrice price = new ProductPrice("1234", 11.23, "USD");
        when(productPriceRepository.findById("1234")).thenReturn(Optional.of(price));

        Product product = productService.getProduct("1234");

        assertNotNull(product);
        assertEquals("1234", product.getId());
        assertEquals("Test Project", product.getName());
        assertEquals(Double.valueOf(11.23), product.getCurrentPrice());
        assertEquals("USD", product.getCurrencyCode());
    }

    @Test(expected = MissingDataException.class)
    public void testProductFetching_MissingData() {
        when(productPriceRepository.findById(anyString())).thenReturn(Optional.empty());
        Product product = productService.getProduct("1234");
    }
}